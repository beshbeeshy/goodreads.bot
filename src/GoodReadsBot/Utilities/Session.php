<?php

namespace GoodReadsBot\Utilities;

class Session
{
    private $sid;

    public function __construct($sid) {
        $this->sid = $sid;
        session_id($this->sid);
        session_start();
        $this->set('sid', $sid);
    }

    public function id() {
        return $this->sid;
    }

    public function set($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function get($key) {
        return $_SESSION[$key] ?? null;
    }

    public function remove($key) {
        unset($_SESSION[$key]);
    }
}