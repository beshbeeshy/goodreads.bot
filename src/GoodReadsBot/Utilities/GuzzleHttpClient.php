<?php

namespace GoodReadsBot\Utilities;

use GuzzleHttp\Client;

class GuzzleHttpClient implements HttpClientInterface
{
    public function request($method, $url, $headers = [], $query = [], $body = null) {
        $options = [
            'allow_redirects' => true
        ];

        if (!empty($headers)) {
            $options['headers'] = $headers;
        }

        if (!empty($query)) {
            $options['query'] = $query;
        }

        if(!empty($body)) {
            $options['body'] = $body;
        }

        $client = new Client();
        return $client->request(
            $method,
            $url,
            $options
        );
    }
}