<?php

namespace GoodReadsBot\Utilities;

class MessengerRequest
{
    private $body;

    public function __construct() {
        $this->body = json_decode(file_get_contents('php://input'), true);
    }

    public function getPSID() {
        return $this->body['entry'][0]['messaging'][0]['sender']['id'];
    }

    public function getQuickReply() {
        return $this->body['entry'][0]['messaging'][0]['message']['quick_reply']['payload'];
    }

    public function getTextResponse() {
        return $this->body['entry'][0]['messaging'][0]['message']['text'];
    }
}