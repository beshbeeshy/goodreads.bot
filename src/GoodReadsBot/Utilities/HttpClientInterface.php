<?php

namespace GoodReadsBot\Utilities;

Interface HttpClientInterface
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    public function request($method, $url, $headers = [], $query = [], $body = null);
}