<?php

namespace GoodReadsBot\States;

class RecommendBookState implements StateInterface
{
    private $messenger;
    private $goodReads;
    private $watson;

    public function __construct($messenger, $goodReads, $watson) {
        $this->messenger = $messenger;
        $this->goodReads = $goodReads;
        $this->watson = $watson;
    }

    public function execute($request, $session) {
        $sid = $session->get('sid');
        $searchType = $session->get('search_type');

        $this->messenger->sendResponse($sid, 'Give me sometime while I check this book for you ...');

        if ($searchType === 'id') {
            $bookId = $request->getTextResponse();
        } elseif ($searchType === 'title') {
            $bookId = $request->getQuickReply();
        } else {
            $this->messenger->sendResponse($sid, 'Seems like there was a mistake in the flow of the conversation, and we need to start again.');
            $this->messenger->sendResponse(
                $sid,
                'Can you please select the search criteria ?',
                [
                    [ 'content_type' => 'text', 'title' => 'By ID', 'payload' => 'id' ],
                    [ 'content_type' => 'text', 'title' => 'By Title', 'payload' => 'title']
                ]
            );
            return 'ChooseSearchTypeState';
        }

        $reviews = $this->goodReads->getBookReviewsByGoodReadsId($bookId);

        $totalAnalyzedReviews = 0;
        $goodReviewsCount = 0;

        foreach ($reviews as $review) {
            $noTagsReview = strip_tags($review);
            if ($this->isEnglishReview($noTagsReview)) {
                $analysis = $this->watson->analyzeTone($noTagsReview);
                if (!isset($analysis['document_tone']['tones'])) { continue; }
                foreach ($analysis['document_tone']['tones'] as $tone) {
                    if ($tone['tone_id'] === 'joy' && $tone['score'] > 0.6) {
                        $goodReviewsCount++;
                        break;
                    }
                }
                $totalAnalyzedReviews++;
            }
        }

        if ($totalAnalyzedReviews === 0) {
            $this->messenger->sendResponse($sid, 'Sorry but I didn\'t find enough reviews to give a solid recommendation ..');
        } else {
            $goodRatio = $goodReviewsCount / $totalAnalyzedReviews;
            if ($goodRatio >= 0.5) {
                $this->messenger->sendResponse($sid, 'I think you should buy this book ..');
            } else {
                $this->messenger->sendResponse($sid, 'Wellll, Why don\'t you find a better book ?');
            }
        }

        return 'GreetingState';
    }

    protected function isEnglishReview($review) {
        return preg_match("/[a-zA-Z0-9\s\p{P}]/", $review);
    }
}