<?php

namespace GoodReadsBot\States;

class ChooseSearchTypeState implements StateInterface
{
    private $messenger;

    public function __construct($messenger) {
        $this->messenger = $messenger;
    }

    public function execute($request, $session) {
        $nextState = null;
        $sid = $session->get('sid');
        $messageText = $request->getQuickReply();

        $session->set('search_type', $messageText);

        if ($messageText === 'id') {
            $nextState = 'RecommendBookState';
            $this->messenger->sendResponse($sid, 'Can you send the book id ?');
        } elseif ($messageText === 'title') {
            $nextState = 'FindBookByTitleState';
            $this->messenger->sendResponse($sid, 'Can you send the book title ?');
        } else {
            $this->messenger->sendResponse(
                $sid,
                'Invalid option, Would you please select another option ?',
                [
                    [ 'content_type' => 'text', 'title' => 'By ID', 'payload' => 'id' ],
                    [ 'content_type' => 'text', 'title' => 'By Title', 'payload' => 'title' ]
                ]
            );
        }

        return $nextState;
        
    }
}