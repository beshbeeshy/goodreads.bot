<?php

namespace GoodReadsBot\States;

class FindBookByTitleState implements StateInterface
{
    private $messenger;
    private $goodReads;

    public function __construct($messenger, $goodReads) {
        $this->messenger = $messenger;
        $this->goodReads = $goodReads;
    }
    public function execute($request, $session) {
        $sid = $session->get('sid');
        $quickReplies = [];

        $books = $this->goodReads->findBooksByTitle($request->getTextResponse());

        foreach ($books as $book) {
            $quickReplies[] = [ 'content_type' => 'text', 'title' => $book['title'], 'payload' => $book['id'] ];
        }

        $this->messenger->sendResponse($sid, 'Would you please select the book from the list ?', $quickReplies);

        return 'RecommendBookState';
    }
}