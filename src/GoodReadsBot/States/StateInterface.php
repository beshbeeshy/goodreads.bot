<?php

namespace GoodReadsBot\States;

interface StateInterface
{
    public function execute($request, $session);
}