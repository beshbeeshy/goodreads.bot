<?php

namespace GoodReadsBot\States;

class GreetingState implements StateInterface
{
    private $messenger;

    public function __construct($messenger) {
        $this->messenger = $messenger;
    }

    public function execute($request, $session) {
        $sid = $session->get('sid');
        $info = $this->messenger->getUserInfo($sid);
        $this->messenger->sendResponse($sid, 'Hello ' . $info['first_name']);
        $this->messenger->sendResponse($sid, 'I am here to help you find a book you like');
        $this->messenger->sendResponse(
            $sid,
            'So, How would you like to search for the book ?',
            [
                [
                    'content_type' => 'text',
                    'title' => 'By ID',
                    'payload' => 'id'
                ],
                [
                    'content_type' => 'text',
                    'title' => 'By Title',
                    'payload' => 'title'
                ]
            ]
        );

        return 'ChooseSearchTypeState';
    }
}