<?php

namespace GoodReadsBot\Services;

use GoodReadsBot\Utilities\GuzzleHttpClient;

class ServiceLocator
{

    public static function createState($stateClass) {
        $stateNameSpace = 'GoodReadsBot\\States\\';
        $stateClassFull = (!empty($stateClass)) ? $stateNameSpace.$stateClass : $stateNameSpace.'GreetingState';

        $stateInstance = null;
        switch ($stateClass) {
            case 'GreetingState':
                $stateInstance = new $stateClassFull(
                    self::createFacebookMessenger()
                );
                break;
            case 'ChooseSearchTypeState':
                $stateInstance = new $stateClassFull(
                    self::createFacebookMessenger()
                );
                break;
            case 'FindBookByTitleState':
                $stateInstance = new $stateClassFull(
                    self::createFacebookMessenger(),
                    self::createGoodReadsService()
                );
                break;
            case 'RecommendBookState':
                $stateInstance = new $stateClassFull(
                    self::createFacebookMessenger(),
                    self::createGoodReadsService(),
                    self::createWatsonService()
                );
                break;
            default:
                $stateInstance = new $stateClassFull(
                    self::createFacebookMessenger()
                );
        }

        return $stateInstance;
    }

    public static function createFacebookMessenger() {
        return new FacebookMessenger(
            new GuzzleHttpClient(),
            'EAAGpuZAT9KKIBAFS85zxlGJVAHZA2f3u3UHc1CmQpYZBSRft4GKdF7ro0wI8plLONr1tE4RKshXskAPZAw1oz9WrYZB23L3sfka0NryZCyX1aq579bpaevAarVh2sGRwOWsKquZC7b6tpPt4SyjhB5EZANia62RQFzYk1j3BHIhyVQZDZD'
        );
    }

    public static function createGoodReadsService() {
        return new GoodReadsService(
            new GuzzleHttpClient(),
            'itfdpGEfzS0yPghGFbsxg'
        );
    }

    public static function createWatsonService() {
        return new WatsonService(
            new GuzzleHttpClient(),
            '42c5aed4-e435-45e4-9761-9605360fab5c',
            'XWRwxwW0eBXo'
        );
    }
}