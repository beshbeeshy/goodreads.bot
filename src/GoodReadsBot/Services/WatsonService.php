<?php

namespace GoodReadsBot\Services;

use GoodReadsBot\Utilities\HttpClientInterface;

class WatsonService
{
    const URL = 'https://gateway.watsonplatform.net';

    private $client;
    private $username;
    private $password;

    public function __construct(HttpClientInterface $client, $username, $password) {
        $this->client = $client;
        $this->username = $username;
        $this->password = $password;
    }

    public function analyzeTone($text) {
        $response = $this->sendHttpRequest(
            '/tone-analyzer/api/v3/tone',
            [
                'version' => '2017-09-21',
                'text' => $text
            ]
        );
        $responseBody = $response->getBody()->getContents();
        return json_decode($responseBody, true);
    }

    private function sendHttpRequest($endPoint, $query) {
        return $this->client->request(
            HttpClientInterface::METHOD_GET,
            self::URL . $endPoint,
            [
                'Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode($this->username.':'.$this->password)
            ],
            $query
        );
    }
}