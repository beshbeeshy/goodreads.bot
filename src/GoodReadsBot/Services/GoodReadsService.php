<?php

namespace GoodReadsBot\Services;

use GoodReadsBot\Utilities\HttpClientInterface;
use \DOMDocument;
use \DOMXPath;
use \SimpleXmlElement;

class GoodReadsService
{
    const URL = 'https://www.goodreads.com';

    private $client;
    private $apiKey;

    public function __construct(HttpClientInterface $client, $apiKey) {
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    public function findBooksByTitle($title, $limit = 5) {
        $booksResponse = $this->sendHttpRequest(
            '/search/index.xml',
            [ 'q' => $title ]
        );
        return $this->parseBooks($booksResponse->getBody()->getContents(), $limit);
    }

    public function getBookReviewsByGoodReadsId($id, $page = 1) {
        $reviews = [];

        $reviewsResponse = $this->sendHttpRequest(
            '/book/delayable_book_show',
            [ 'id' => $id, 'sort' => 'newest', 'text_only' => 'true', 'page' => $page ]
        );

        $reviewsDocument = new DOMDocument("1.0", "UTF-8");
        $reviewsDocument->loadHTML(
            mb_convert_encoding($reviewsResponse->getBody()->getContents(), 'HTML-ENTITIES', 'UTF-8')
        );
        $reviewsDocument->normalizeDocument();

        $domXpath = new DOMXPath($reviewsDocument);

        $bookReviews = $domXpath->query(".//span[@class='readable']");

        foreach ($bookReviews as $review) {
            $reviews[] = $review->childNodes->item(3)->nodeValue ?? $review->childNodes->item(1)->nodeValue;
        }

        return $reviews;
    }

    private function parseBooks($response, $limit) {
        $books = [];
        $parsedResponse = new SimpleXmlElement($response);
        $booksList = $parsedResponse->xpath(
            sprintf('(.//best_book)[position() < %d]', $limit+1)
        );

        foreach ($booksList as $bookData) {
            $books[] = [
                'id' => (string) $bookData->id,
                'title' => (string) $bookData->title
            ];
        }

        return $books;
    }

    private function parseReviews() {

    }

    private function sendHttpRequest($endPoint, $query) {
        $query['key'] = $this->apiKey;

        return $this->client->request(
            HttpClientInterface::METHOD_GET,
            self::URL . $endPoint,
            [],
            $query
        );
    }
}