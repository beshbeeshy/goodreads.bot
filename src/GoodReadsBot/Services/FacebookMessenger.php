<?php

namespace GoodReadsBot\Services;

use GoodReadsBot\Utilities\HttpClientInterface;

class FacebookMessenger
{
    const URL = 'https://graph.facebook.com';
    const MESSAGES_ENDPOINT = '/v2.6/me/messages';

    private $client;
    private $accessToken;

    public function __construct(HttpClientInterface $client, $accessToken) {
        $this->client = $client;
        $this->accessToken = $accessToken;
    }

    public function getUserInfo($uid) {
        $response = $this->sendHttpRequest(HttpClientInterface::METHOD_GET, '/' . $uid, [ 'fields' => 'first_name,last_name' ]);
        return json_decode($response->getBody()->getContents(), true);
    }

    public function sendResponse($userId, $message, $replies = []) {
        $body = [
            'messaging_type' => 'RESPONSE',
            'recipient' => [
                'id' => $userId
            ],
            'message' => [
                'text' => $message
            ]
        ];

        if (!empty($replies)) {
            $body['message']['quick_replies'] = $replies;
        }

        $this->sendHttpRequest(HttpClientInterface::METHOD_POST, self::MESSAGES_ENDPOINT, $body);
    }

    private function sendHttpRequest($method, $endPoint, $query, $body = null) {
        $query['access_token'] = $this->accessToken;

        return $this->client->request(
            $method,
            self::URL . $endPoint,
            [ 'Content-Type' => 'application/json' ],
            $query,
            json_encode($body)
        );
    }
}