<?php

namespace Tests\States;

use GoodReadsBot\Services\FacebookMessenger;
use GoodReadsBot\Services\GoodReadsService;
use GoodReadsBot\States\FindBookByTitleState;
use GoodReadsBot\Utilities\MessengerRequest;
use GoodReadsBot\Utilities\Session;
use \Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

class FindBookByTitleStateTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private $mockedMessenger;
    private $mockedGoodReadsService;
    private $mockedMessengerRequest;
    private $findBookByTitleState;

    private $sid = '123456789';

    public function setUp() {
        $this->mockedMessenger = Mockery::mock(FacebookMessenger::class);
        $this->mockedGoodReadsService = Mockery::mock(GoodReadsService::class);
        $this->mockedMessengerRequest = Mockery::mock(MessengerRequest::class);
        $this->mockedSession = Mockery::mock(Session::class);
        $this->findBookByTitleState = new FindBookByTitleState(
            $this->mockedMessenger,
            $this->mockedGoodReadsService
        );

        $this->mockedSession->shouldReceive('get')
            ->with('sid')
            ->andReturn($this->sid);
    }

    public function testFindBookByTitle() {
        $mockedBookList = [
            ['id' => '1', 'title' => 'Learning Spark'],
            ['id' => '2', 'title' => 'Spark for dummies']
        ];
        $this->mockedMessengerRequest->shouldReceive('getTextResponse')
            ->andReturn('Learning Spark');
        $this->mockedGoodReadsService->shouldReceive('findBooksByTitle')
            ->andReturn($mockedBookList);
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with(
                $this->sid,
                'Would you please select the book from the list ?',
                [
                    ['content_type' => 'text', 'title' => 'Learning Spark', 'payload' => '1'],
                    ['content_type' => 'text', 'title' => 'Spark for dummies', 'payload' => '2']
                ]
            );

        $nextState = $this->findBookByTitleState->execute($this->mockedMessengerRequest, $this->mockedSession);
        $this->assertEquals('RecommendBookState', $nextState);
    }
}