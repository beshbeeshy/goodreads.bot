<?php

namespace Tests\States;

use GoodReadsBot\Services\FacebookMessenger;
use GoodReadsBot\States\ChooseSearchTypeState;
use GoodReadsBot\Utilities\MessengerRequest;
use GoodReadsBot\Utilities\Session;
use \Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

class ChooseSearchTypeStateTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private $mockedMessenger;
    private $mockedMessengerRequest;
    private $chooseSearchTypeState;

    private $sid = '123456789';

    public function setUp() {
        $this->mockedMessenger = Mockery::mock(FacebookMessenger::class);
        $this->mockedMessengerRequest = Mockery::mock(MessengerRequest::class);
        $this->mockedSession = Mockery::mock(Session::class);
        $this->chooseSearchTypeState = new ChooseSearchTypeState($this->mockedMessenger);

        $this->mockedSession->shouldReceive('get')
            ->with('sid')
            ->andReturn($this->sid);
    }

    public function testSearchById() {
        $this->mockedMessengerRequest->shouldReceive('getQuickReply')
            ->andReturn('id');
        $this->mockedSession->shouldReceive('set')
            ->with('search_type', 'id');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, 'Can you send the book id ?');

        $nextState = $this->chooseSearchTypeState->execute($this->mockedMessengerRequest, $this->mockedSession);
        $this->assertEquals('RecommendBookState', $nextState);
    }

    public function testSearchByTitle() {
        $this->mockedMessengerRequest->shouldReceive('getQuickReply')
            ->andReturn('title');
        $this->mockedSession->shouldReceive('set')
            ->with('search_type', 'title');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, 'Can you send the book title ?');

        $this->chooseSearchTypeState->execute($this->mockedMessengerRequest, $this->mockedSession);
    }

    public function testSearchByInvalidOption() {
        $this->mockedMessengerRequest->shouldReceive('getQuickReply')
            ->andReturn('invalid');
        $this->mockedSession->shouldReceive('set')
            ->with('search_type', 'invalid');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with(
                $this->sid,
                'Invalid option, Would you please select another option ?',
                [
                    [ 'content_type' => 'text', 'title' => 'By ID', 'payload' => 'id' ],
                    [ 'content_type' => 'text', 'title' => 'By Title', 'payload' => 'title' ]
                ]
            );

        $this->chooseSearchTypeState->execute($this->mockedMessengerRequest, $this->mockedSession);
    }
}