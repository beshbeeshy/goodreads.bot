<?php

namespace Tests\States;

use GoodReadsBot\Services\FacebookMessenger;
use GoodReadsBot\Services\GoodReadsService;
use GoodReadsBot\Services\WatsonService;
use GoodReadsBot\States\RecommendBookState;
use GoodReadsBot\Utilities\MessengerRequest;
use GoodReadsBot\Utilities\Session;
use \Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

class RecommendBookStateTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private $mockedMessenger;
    private $mockedGoodReadsService;
    private $mockedWatsonService;
    private $mockedMessengerRequest;
    private $recommendBookState;

    private $sid = '123456789';
    private $bookId = '1234';
    private $reviewText1 = 'This is the best book I have ever read';
    private $reviewText2 = 'This book has changed my life :D';
    private $reviewText3 = 'كلام مش انجليزى';
    private $happyRecommendation = 'I think you should buy this book ..';
    private $badRecommendation = 'Wellll, Why don\'t you find a better book ?';

    public function setUp() {
        $this->mockedMessenger = Mockery::mock(FacebookMessenger::class);
        $this->mockedGoodReadsService = Mockery::mock(GoodReadsService::class);
        $this->mockedWatsonService = Mockery::mock(WatsonService::class);
        $this->mockedMessengerRequest = Mockery::mock(MessengerRequest::class);
        $this->mockedSession = Mockery::mock(Session::class);
        $this->recommendBookState = new RecommendBookState($this->mockedMessenger, $this->mockedGoodReadsService, $this->mockedWatsonService);

        $this->mockedSession->shouldReceive('get')
            ->with('sid')
            ->andReturn($this->sid);
    }

    /**
     * @dataProvider provideBookAnalysisData
     */
    public function testRecommendBookState($searchType, $getResponseMethod, $review1Analysis, $review2Analysis, $review3Analysis, $userRecommendation) {
        $this->mockedSession->shouldReceive('get')
            ->with('search_type')
            ->andReturn($searchType);
        $this->mockedMessengerRequest->shouldReceive($getResponseMethod)
            ->andReturn($this->bookId);
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, 'Give me sometime while I check this book for you ...');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, $userRecommendation);
        $this->mockedGoodReadsService->shouldReceive('getBookReviewsByGoodReadsId')
            ->with($this->bookId)
            ->andReturn([ $this->reviewText1, $this->reviewText2 ]);
        $this->mockedWatsonService->shouldReceive('analyzeTone')
            ->with($this->reviewText1)
            ->andReturn($review1Analysis);
        $this->mockedWatsonService->shouldReceive('analyzeTone')
            ->with($this->reviewText2)
            ->andReturn($review2Analysis);
        $this->mockedWatsonService->shouldReceive('analyzeTone')
            ->with($this->reviewText3)
            ->andReturn($review3Analysis);

        $nextState = $this->recommendBookState->execute($this->mockedMessengerRequest, $this->mockedSession);
        $this->assertEquals('GreetingState', $nextState);
    }

    public function testRecommendBookStateWithNoReviews() {
        $this->mockedSession->shouldReceive('get')
            ->with('search_type')
            ->andReturn('id');
        $this->mockedMessengerRequest->shouldReceive('getTextResponse')
            ->andReturn($this->bookId);
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, 'Give me sometime while I check this book for you ...');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, 'Sorry but I didn\'t find enough reviews to give a solid recommendation ..');
        $this->mockedGoodReadsService->shouldReceive('getBookReviewsByGoodReadsId')
            ->with($this->bookId)
            ->andReturn([]);

        $nextState = $this->recommendBookState->execute($this->mockedMessengerRequest, $this->mockedSession);
        $this->assertEquals('GreetingState', $nextState);
    }

    public function testRecommendBookStateWithInvalidSearchType() {
        $this->mockedSession->shouldReceive('get')
            ->with('search_type')
            ->andReturn('invalid');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, 'Give me sometime while I check this book for you ...');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, 'Seems like there was a mistake in the flow of the conversation, and we need to start again.');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with(
                $this->sid,
                'Can you please select the search criteria ?',
                [
                    [ 'content_type' => 'text', 'title' => 'By ID', 'payload' => 'id' ],
                    [ 'content_type' => 'text', 'title' => 'By Title', 'payload' => 'title']
                ]
            );

        $nextState = $this->recommendBookState->execute($this->mockedMessengerRequest, $this->mockedSession);
        $this->assertEquals('ChooseSearchTypeState', $nextState);
    }


    public function provideBookAnalysisData() {
        return [
            ['id', 'getTextResponse', $this->getHappyReviewAnalysis(), $this->getHappyReviewAnalysis(), $this->getHappyReviewAnalysis(), $this->happyRecommendation],
            ['id', 'getTextResponse', $this->getBadReviewAnalysis(), $this->getBadReviewAnalysis(), $this->getBadReviewAnalysis(), $this->badRecommendation],
            ['title', 'getQuickReply', $this->getBadReviewAnalysis(), $this->getBadReviewAnalysis(), $this->getBadReviewAnalysis(), $this->badRecommendation]
        ];
    }

    private function getHappyReviewAnalysis() {
        $response = <<<RESPONSE
        {
            "document_tone" : {
                "tones" : [
                    { "score" : 0.616588, "tone_id" : "sadness", "tone_name" : "Sadness" },
                    { "score" : 0.829888, "tone_id" : "analytical", "tone_name" : "Analytical" },
                    { "score" : 0.829888, "tone_id" : "joy", "tone_name" : "Joy" }
                ]
            }
        }
RESPONSE;
        return json_decode($response, true);
    }

    private function getBadReviewAnalysis() {
        $response = <<<RESPONSE
        {
            "document_tone" : {
                "tones" : [
                    { "score" : 0.616588, "tone_id" : "sadness", "tone_name" : "Sadness" },
                    { "score" : 0.829888, "tone_id" : "analytical", "tone_name" : "Analytical" },
                    { "score" : 0.229888, "tone_id" : "joy", "tone_name" : "Joy" }
                ]
            }
        }
RESPONSE;
        return json_decode($response, true);
    }
}