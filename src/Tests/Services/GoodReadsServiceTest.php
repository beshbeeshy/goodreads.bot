<?php

namespace Tests\Services;

use GoodReadsBot\Services\GoodReadsService;
use GoodReadsBot\Utilities\HttpClientInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use \Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

class GoodReadsServiceTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private $mockedHttpClient;
    private $mockedResponse;
    private $mockedStream;
    private $goodReadsService;
    private $bookId = '1234';
    private $apiKey = 'ABCD1234';

    public function setUp() {
        $this->mockedHttpClient = Mockery::mock(HttpClientInterface::class);
        $this->mockedResponse = Mockery::mock(Response::class);
        $this->mockedStream = Mockery::mock(Stream::class);
        $this->goodReadsService = new GoodReadsService($this->mockedHttpClient, $this->apiKey);
    }

    public function testParsingReviews() {
        $this->mockedStream->shouldReceive('getContents')
            ->andReturn(
                file_get_contents(
                    __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'SampleGoodReadsReviewsResponse.txt'
                )
            );
        $this->mockedResponse->shouldReceive('getBody')
            ->andReturn($this->mockedStream);
        $this->mockedHttpClient->shouldReceive('request')
            ->with(
                'GET',
                'https://www.goodreads.com/book/delayable_book_show',
                [],
                ['id' => $this->bookId, 'sort' => 'newest', 'text_only' => 'true', 'page' => 1, 'key' => $this->apiKey]
            )
            ->andReturn($this->mockedResponse);
        $reviews = $this->goodReadsService->getBookReviewsByGoodReadsId($this->bookId, 1);
        $this->assertEquals(30, count($reviews));
    }
}