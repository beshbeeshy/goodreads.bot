<?php

namespace Tests;

use GoodReadsBot\Services\FacebookMessenger;
use GoodReadsBot\States\GreetingState;
use GoodReadsBot\Utilities\MessengerRequest;
use GoodReadsBot\Utilities\Session;
use \Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

class GreetingStateTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private $mockedMessenger;
    private $mockedMessengerRequest;
    private $greetingState;

    private $sid = '123456789';

    public function setUp() {
        $this->mockedMessenger = Mockery::mock(FacebookMessenger::class);
        $this->mockedMessengerRequest = Mockery::mock(MessengerRequest::class);
        $this->mockedSession = Mockery::mock(Session::class);
        $this->greetingState = new GreetingState($this->mockedMessenger);

        $this->mockedSession->shouldReceive('get')
            ->with('sid')
            ->andReturn($this->sid);
    }

    public function testSayHello() {
        $this->mockedMessengerRequest->shouldReceive('getTextResponse')
            ->andReturn('Learning Spark');
        $this->mockedMessenger->shouldReceive('getUserInfo')
            ->with($this->sid)
            ->andReturn(['first_name' => 'Bassam', 'last_name' => 'Ahmed']);
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, 'Hello Bassam');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with($this->sid, 'I am here to help you find a book you like');
        $this->mockedMessenger->shouldReceive('sendResponse')
            ->with(
                $this->sid,
                'So, How would you like to search for the book ?',
                [
                    [ 'content_type' => 'text', 'title' => 'By ID', 'payload' => 'id' ],
                    [ 'content_type' => 'text', 'title' => 'By Title', 'payload' => 'title' ]
                ]
            );

        $nextState = $this->greetingState->execute($this->mockedMessengerRequest, $this->mockedSession);
        $this->assertEquals('ChooseSearchTypeState', $nextState);
    }
}